<?php
use Phalcon\Mvc\Model;

class Person extends Model
{
    public function initialize()
    {
        $this->setSource('persons');
    }
}
