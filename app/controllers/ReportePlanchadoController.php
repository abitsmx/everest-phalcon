<?php

use Phalcon\Mvc\Controller;

class ReportePlanchadoController extends Controller
{
    public function indexAction ()
    {

    }

    public function getWhereString($arr)
    {
        $where = '( ';
        $i = 0;

        foreach ($arr as $val)
        {

            $where .= ($i > 0) ? ' OR ' : '';
            $where .= ' job LIKE "%' . $val . '%"';
            $i++;

        }

        $where .= ' )';

        return $where;
    }


    public function personsByRol()
    {
        $builder = $this->modelsManager->createBuilder();

        $builder->columns([ 'total' => 'COUNT(*)', 'role' ])
        ->from('Person')
        ->orderBy('total DESC')
        ->groupBy('role');

        return $builder->getQuery()->execute();
    }


    public function personsByRolAction ()
    {
        $r = $this->personsByRol();
        return $this->response->setJsonContent([
            'data' => $r->toArray()
        ]);
    }



    public function calcularCambiosAction ()
    {
        $input = $this->request->getJsonRawBody();

        try
        {

            $builder = $this->modelsManager->createBuilder();
            $builder->columns(['total' => 'COUNT(*)'])->from('Person');

            $where = $this->getWhereString($input->palabrasClave);
            $builder->andWhere($where);

            $simples = $builder->getQuery()->execute();



            $builder = $this->modelsManager->createBuilder();
            $builder->columns(['total' => 'COUNT(*)'])->from('Person');

            $where2 = $this->getWhereString($input->palabrasCompuestas);
            $builder->andWhere($where)->andWhere($where2);

            $compuestas = $builder->getQuery()->execute();
        }
        catch(\Exception $e)
        {
            echo $e->getMessage();
        }

        return $this->response->setJsonContent([
            'data' => [
                'coincide' => [
                    'simples'    => $simples[0]? $simples[0]->total : null,
                    'compuestas' => $compuestas[0]? $compuestas[0]->total : null
                ]
            ]
        ]);
    }



    public function aplicarCambiosAction ()
    {
        $input  = $this->request->getJsonRawBody();
        $where  = $this->getWhereString($input->palabrasClave);
        $where2 = $this->getWhereString($input->palabrasCompuestas);
        $sql    = "UPDATE persons SET role = '{$input->role}' WHERE  " . $where;
        $sql2   = "UPDATE persons SET role = '{$input->role2}' WHERE " . $where . " AND " .$where2;

        $before = $this->personsByRol()->toArray();

        $this->db->query($sql);
        $this->db->query($sql2);

        return $this->response->setJsonContent([
            'data' => [
                'updateRol' => $sql,
                'updateRol2'=> $sql2,
                'before'    => $before,
                'after'     => $this->personsByRol()->toArray()
            ]
        ]);
    }

    public function fixRoles ($params)
    {
        $palabrasClave = $params->palabrasClave;

    }
}
