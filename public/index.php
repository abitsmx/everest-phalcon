<?php

use Phalcon\Loader;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Application;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;

require_once(__DIR__ . '/../../env.php');

error_reporting(E_ALL);
ini_set('display_errors',1);


// Register an autoloader
$loader = new Loader();
$loader->registerDirs(
    [
        "../app/controllers/",
        "../app/models/",
    ]
);

$loader->register();


// Create a DI
$di = new FactoryDefault();

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set("db", function () {
    return new DbAdapter(array(
        "host"      => DB_HOST,
        "username"  => DB_USER,
        "password"  => DB_PASSWORD,
        "dbname"    => DB_DATABASE,
        "charset"   => "utf8",
        "options"   => [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]
    ));
});

// Setup the view component
$di->set("view",function () {
        $view = new View();
        $view->setViewsDir("../app/views/");
        return $view;
    });

// Setup a base URI so that all generated URIs include the "tutorial" folder
$di->set("url",function () {
        $url = new UrlProvider();

        $url->setBaseUri("/ph/");

        return $url;
    });


$application = new Application($di);

try {
    // Handle the request
    $response = $application->handle();

    $response->send();
} catch (\Exception $e) {
    echo "Exception: ", $e->getMessage();
}
